 <!DOCTYPE html>
<html lang="de">
	<head>
		<meta charset="UTF-8">
		<title>DGN#FOG Editor</title>
		
		<!-- JQuery -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
		<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		
		<!-- Bootstrap -->
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		
		<!-- Bootstrap-Touchspin -->
		<link rel="stylesheet" href="res/vendor/touchspin/jquery.bootstrap-touchspin.css">
		<script type="text/javascript" src="res/vendor/touchspin/jquery.bootstrap-touchspin.js"></script>
		
		<!-- Font-Awesome -->
		<link rel="stylesheet" href="res/vendor/font-awesome/css/font-awesome.min.css">
		
		<!-- PHP-Less-Compiler -->
		<?php
			require "res/vendor/lessphp/lessc.inc.php";
			$less = new lessc;
			$less->checkedCompile("res/less/default.less", "res/css/default.css");
		?>
		<link rel="stylesheet" href="res/css/default.css" />
		
		<!-- JS -->
		<script type="text/javascript" src="res/js/default.js"></script>
	</head>
	
	<body>
		<?php
			/*
			 * Textures
			 */
			$backgroundPath = './res/textures/bg';
			$defaultBGPattern = 'grass_01.jpg';
			$bgModalContent = '';
			foreach(scandir($backgroundPath) as $el){
			  if($el != '.' AND $el != '..'){
				$patternPath = $backgroundPath.'/'.$el;
				$bgModalContent .= "<div class='pattern_wrapper'><div class='pattern lg' data-selector='background_pattern' data-pattern='$el' style='background-image:url(\"$patternPath\")'></div></div>";
			  }
			}
			
			$floorPath = './res/textures/floor';
			$defaultFloorPattern = 'wood_01.jpg';
			$floorModalContent = '';
			foreach(scandir($floorPath) as $el){
			  if($el != '.' AND $el != '..'){
				$patternPath = $floorPath.'/'.$el;
				$floorModalContent .= "<div class='pattern_wrapper'><div class='pattern lg' data-selector='floor_pattern' data-pattern='$el' style='background-image:url(\"$patternPath\")'></div></div>";
			  }
			}
			
			$wallPath = './res/textures/wall';
			$defaultWallPattern = 'stone_wall_01.jpg';
			$wallModalContent = '';
			foreach(scandir($wallPath) as $el){
			  if($el != '.' AND $el != '..'){
				$patternPath = $wallPath.'/'.$el;
				$wallModalContent .= "<div class='pattern_wrapper'><div class='pattern lg' data-selector='wall_pattern' data-pattern='$el' style='background-image:url(\"$patternPath\")'></div></div>";
			  }
			}
			
			/*
			 * Props
			 */
			$propPath = './res/props/props';
			$propContent = '<div class="proplist">';
			$i = 0;
			foreach(scandir($propPath) as $el){
				$i++;
				if($el != '.' AND $el != '..'){
					
					$subpath = $propPath.'/'.$el;
					#$propContent .= '<div class="proplist_headline">'.$el.'<span class="glyphicon glyphicon-plus"></span></div>';
					$propContent .= '<div class="proplist_headline">'.$el.'<i class="fa fa-plus"></i></div>';
					$propContent .= '<div class="proplist_content">';
					
					foreach(scandir($subpath) as $el2){
						
						if($el2 != '.' AND $el2 != '..'){
							$name = explode('_', $el2);
							$name = $name[0];
							$patternPath = $subpath.'/'.$el2;
							$propContent .= "<div class='prop'>
												<div class='prop_inner'>
													<div class='prop_img' style='background-image: url(\"$patternPath\");' ></div>
													<span class='prop_title'>$name</span>
												</div>
											</div>";
						}
					}
					$propContent .= '</div>';
					if($i != sizeof(scandir($propPath))){
						$propContent .= '</div><div class="proplist">';
					}
				}
			}
			$propContent .= '</div>';
			
			/*
			 * Doors
			 */
			$doorPath = './res/props/doors';
			$doorContent = '<div class="proplist">
								<div class="proplist_content door">';
			foreach(scandir($doorPath) as $el){
				if($el != '.' AND $el != '..'){
					
					$patternPath = $doorPath.'/'.$el;
					
					$name = explode('_', $el);
					$name = $name[0];
					$doorContent .= "<div class='prop door'>
										<div class='prop_inner'>
											<div class='prop_img' style='background-image: url(\"$patternPath\");' ></div>
											<span class='prop_title'>$name</span>
										</div>
									</div>";
				}
			}
			$doorContent .= '</div></div>';
			
			/*
			 * Traps
			 */
			$trapPath = './res/props/traps';
			$trapContent = '<div class="proplist">
								<div class="proplist_content door">';
			foreach(scandir($trapPath) as $el){
				if($el != '.' AND $el != '..'){
					
					$patternPath = $trapPath.'/'.$el;
					
					$name = explode('_', $el);
					$name = $name[0];
					$trapContent .= "<div class='prop door'>
										<div class='prop_inner'>
											<div class='prop_img' style='background-image: url(\"$patternPath\");' ></div>
											<span class='prop_title'>$name</span>
										</div>
									</div>";
				}
			}
			$trapContent .= '</div></div>';
			
			
		?>
		<div id="editor">
			<div id="top">
				<div id="map_name" class="col-sm-3">
					<input type="text" name="map_name" value="Map01"/>
					<div id="name_line"></div>
				</div>
				<div id="map_functions" class="col-md-6">
					<ul>
						<li><a href="#">Preview</a></li>
						<li><a href="#">Save</a></li>
						<li><a href="#">Save at</a></li>
						<li><a href="#">Save & Exit</a></li>
						<li><a href="#">Exit without saving</a></li>
					</ul>
				</div>
				<div id="logo" class="col-sm-3">
					<img src="res/img/logo.png" alt="dungeon fog logo"/>
				</div>
			</div>
			<div id="toolbar">
				<div id="properties">
					<!-- draw line properties -->
					<label>Line</label>
					<input type="text" class="form-control" value="11" name="line_thickness" maxlength="4" />
					<div id="line_stroke" class="line_type active"></div>
					<div id="line_dotted" class="line_type"></div>
					
					<label>Wall</label>
					<button type="button" class="" data-toggle="modal" data-target="#wallModal">
						<div class="wall_pattern pattern" data-pattern="<?php echo $defaultWallPattern; ?>" style='background-image:url("<?php echo $wallPath.'/'.$defaultWallPattern ?>")'></div>
					</button>
					
					<label>Outline</label>
					<input class="pattern" type="color" name="outline_color" value="#000000" />
					
					<label>Floor</label>
					<button type="button" class="" data-toggle="modal" data-target="#floorModal">
						<div class="floor_pattern pattern" data-pattern="<?php echo $defaultFloorPattern; ?>" style='background-image:url("<?php echo $floorPath.'/'.$defaultFloorPattern ?>")'></div>
					</button>
					
					<div class="toggle_shadow">
						<span class="shadow_text">Shadow</span>
						<span class="shadow_on">ON</span>
					</div>
				</div>
				<div id="zoom">
					<div id="cat_collapse"></div>
					<div id="input_wrapper">
						<input type="range" min="0" max="10" step="1" name="zoom_stage" value="5">
					</div>
				</div>
			</div>
			<div id="main" class="clearfix">
				<div id="main_tools">
					<div class="tool_icon" id="tool_selector"></div>
					<div class="tool_icon" id="tool_draw_line"></div>
					<div class="tool_icon" id="tool_draw_square"></div>
					<div class="tool_icon" id="tool_draw_circle"></div>
					<div class="tool_icon disabled" id="tool_draw_floor"></div>
					<div class="tool_icon disabled" id="tool_draw_pattern"></div>
				</div>
				<div id="main_stage"></div>
				<div id="option_cats">
					<div class="option_cat" id="cat_settings"></div>
					<div class="option_cat" id="cat_rooms"></div>
					<div class="option_cat" id="cat_props"></div>
					<div class="option_cat" id="cat_doors"></div>
					<div class="option_cat" id="cat_stairs"></div>
					<div class="option_cat" id="cat_traps"></div>
					<div class="option_cat" id="cat_notes"></div>
					<div class="option_cat" id="cat_links"></div>
					<div class="option_cat disabled" id="cat_sounds"></div>
				</div>
				<div id="options">
					<div id="options_inner">
						<div id="options_settings">
							<div class="options_headline">Map Settings</div>
							<div class="options_set">
								<label>Setting</label>
								<select name="setting" class="form-control">
									<option value="fantasy">Fantasy</option>
									<option value="modern">Modern</option>
									<option value="sci-fi">Sci-Fi</option>
									<option value="horror">Horror</option>
									<option value="apocalypse">Apocalypse</option>
									<option value="superheros">Superheros</option>
								</select>
							</div>
							<div class="options_set">
								<label>Grid Type</label>
								<select name="grid_type" class="form-control">
									<option value="square">Square</option>
									<option value="hex">Hex</option>
									<option value="triangle">Triangle</option>
								</select>
							</div>
							<div class="options_set">
								<label>Units</label>
								<input type="text" class="form-control" name="map_width" value="25"/>
								<span>x</span>
								<input type="text" class="form-control" name="map_height" value="25"/>
							</div>
							<div class="options_set">
								<label>Grid Size</label>
								<input type="text" class="form-control" name="grid_width" value="50"/>
								<span>x</span>
								<input type="text" class="form-control" name="grid_height" value="50"/>
							</div>
							<div class="options_set">
								<label>1U</label>
								<input type="text" class="form-control" name="unit_distance" value="5"/>
								<select name="unit_measure" class="form-control">
									<option value="ft">ft</option>
									<option value="m">m</option>
								</select>
							</div>
							<!--
							<div class="options_set">
								<label>Snap to grid</label>
								<input type="checkbox" class="form-control" name="snap_to_grid" value="on" />
							</div>
							-->
							<div class="options_set">
								<label>Background</label>
								<select name="background_type" class="form-control">
									<option value="pattern">pattern</option>
									<option value="color">color</option>
									<option value="image">image</option>
								</select>
							</div>
							<div class="options_set">
								<label>Pattern</label>
								<button type="button" class="" data-toggle="modal" data-target="#bgModal">
									<div class="pattern background" data-pattern="<?php echo $defaultBGPattern; ?>" style='background-image:url("<?php echo $backgroundPath.'/'.$defaultBGPattern ?>")' id="background_pattern"></div>
								</button>
								<!-- Style pointer-events l�sst klicks nicht reagieren (auf css seite)
								<button type="button"  style="pointer-events: none" data-toggle="modal" data-target="#bgModal">
									<div class="pattern background" data-pattern="<?php echo $defaultBGPattern; ?>" style='background-image:url("<?php echo $backgroundPath.'/'.$defaultBGPattern ?>")' id="background_pattern"></div>
								</button>
								-->
								<input type="text" class="form-control" name="background_patter_size" value="100%" />
							</div>
							<div class="options_set disabled">
								<label>Color</label>
								<input class="pattern" type="color" name="background_color" value="#000000" disabled="disabled"/>
							</div>
							<div class="options_set">
								<div class="input-group">
									<label class="input-group-btn">
										<span class="btn btn-default disabled">
											choose image <input style="display: none;" multiple="" type="file" disabled="disabled">
										</span>
									</label>
									<input class="form-control" name="choose_bg_file" readonly="" type="text" disabled="disabled">
								</div>
							</div>
						</div>
						<div id="options_rooms">
							<div class="options_headline">Room Settings</div>
							<div class="options_set">
								<label>Line</label>
								<input type="text" class="form-control" name="line_thickness" value="11"/>
								<div id="line_types">
									<div id="line_stroke_side" class="line_type active"></div>
									<div id="line_dotted_side" class="line_type"></div>
								</div>
							</div>
							<div class="options_set">
								<label>Wall</label>
								<button type="button" class="" data-toggle="modal" data-target="#wallModal">
									<div class="wall_pattern pattern" data-pattern="<?php echo $defaultWallPattern; ?>" style='background-image:url("<?php echo $wallPath.'/'.$defaultWallPattern ?>")'></div>
								</button>
								<input type="text" class="form-control" name="wall_pattern_scale" value="100%"/>
							</div>
							<div class="options_set">
								<label>Outline</label>
								<input class="pattern" type="color" name="outline_color" value="#000000" />
							</div>
							<div class="options_set">
								<label>Floor</label>
								<button type="button" class="" data-toggle="modal" data-target="#floorModal">
									<div class="floor_pattern pattern" data-pattern="<?php echo $defaultFloorPattern; ?>" style='background-image:url("<?php echo $floorPath.'/'.$defaultFloorPattern ?>")'></div>
								</button>
								<input type="text" class="form-control" name=floor_pattern_scale" value="100%"/>
							</div>
							<div class="options_set">
								<label>Shadow</label>
								<input type="checkbox" class="form-control" name="toggle_shadow" value="on" checked="checked" />
							</div>
							<div class="advanced_options_wrapper">
								<div class="advanced_options_label">Advanced Options <i class="fa fa-plus"></i></div>
								<div class="advanced_options_inner">
									<div class="options_headline">Properties of</div>
									<div class="options_set">
										<label>Number</label>
										<input type="text" class="form-control single" name="prop_number" value="1" />
									</div>
									<div class="options_set">
										<label>Name</label>
										<input type="text" class="form-control name single" name="room_name" value="Room01" />
									</div>
									<div class="options_set">
										<label>Position</label>
										<div class="btn-group btn-group-xs" role="group" aria-label="...">
											<button type="button" class="btn btn-default"><i class="fa fa-chevron-up"></i></button>
											<button type="button" class="btn btn-default"><i class="fa fa-chevron-down"></i></button>
										</div>
									</div>
									<div class="options_set">
										<label>Always visible</label>
										<input type="checkbox" class="form-control" name="always_visible" value="on" checked="checked" />
									</div>
									<!--
									<div class="options_set">
										<label>Coord</label>
										<span>x</span><div class="touchspin_wrapper"><input type="text" class="form-control touchspin" name="room_x_pos" value="150"></div>
										<span>y</span><div class="touchspin_wrapper"><input type="text" class="form-control touchspin" name="room_y_pos" value="150"></div>
									</div>
									-->
								</div>
							</div>
						</div>
						<div id="options_props">
							<div class="options_headline">Props</div>
							<div class="options_info">select and click to place</div>
							<div class="proplist_wrapper">
								<?php echo $propContent; ?>
							</div>
							
							<div class="advanced_options_wrapper">
								<div class="advanced_options_label">Advanced Options <i class="fa fa-plus"></i></div>
								<div class="advanced_options_inner">
									<div class="options_headline">Properties of</div>
									<div class="options_set">
										<label>Number</label>
										<input type="text" class="form-control single" name="prop_number" value="1" />
									</div>
									<div class="options_set">
										<label>Name</label>
										<input type="text" class="form-control name single" name="prop_name" value="Prop01" />
									</div>
									<div class="options_set">
										<label>Position</label>
										<div class="btn-group btn-group-xs" role="group" aria-label="...">
											<button type="button" class="btn btn-default"><i class="fa fa-chevron-up"></i></button>
											<button type="button" class="btn btn-default"><i class="fa fa-chevron-down"></i></button>
										</div>
									</div>
									<div class="options_set">
										<label>Always visible</label>
										<input type="checkbox" class="form-control" name="always_visible" value="on" checked="checked" />
									</div>
									<div class="options_set">
										<label>Discover DC</label>
										<input type="text" class="form-control single" name="discover_dc" value="15"/>
									</div>
									<div class="options_set">
										<label>Hitpoints</label>
										<input type="text" class="form-control single" name="hitpoints" value="20"/>
									</div>
									<div class="options_set">
										<label>Is locked</label>
										<input type="checkbox" class="form-control" name="locked" value="on" checked="checked" />
									</div>
									<div class="options_set">
										<label>Locked DC</label>
										<input type="text" class="form-control single" name="locked_dc" value="15"/>
									</div>
									<div class="options_set">
										<label>Is trapped</label>
										<input type="checkbox" class="form-control" name="trapped" value="on" checked="checked" />
									</div>
									<div class="options_set">
										<label>Trap detect DC</label>
										<input type="text" class="form-control single" name="trap_detect_dc" value="15"/>
									</div>
									<div class="options_set">
										<label>Trap disarm DC</label>
										<input type="text" class="form-control single" name="trap_disarm_dc" value="15"/>
									</div>
									<div class="options_set">
										<label class="lg">Description</label>
										<textarea class="form-control" name="description">Prop description</textarea>
									</div>
								</div>
							</div>
						</div>
						<div id="options_doors">
							<div class="options_headline">Doors</div>
							<div class="options_info">select and click to place</div>
							<div class="proplist_wrapper">
								<?php echo $doorContent; ?>
							</div>
							
							<div class="advanced_options_wrapper">
								<div class="advanced_options_label">Advanced Options <i class="fa fa-plus"></i></div>
								<div class="advanced_options_inner">
									<div class="options_headline">Properties of</div>
									<div class="options_set">
										<label>Number</label>
										<input type="text" class="form-control single" name="door_number" value="1" />
									</div>
									<div class="options_set">
										<label>Name</label>
										<input type="text" class="form-control name single" name="door_name" value="Door01" />
									</div>
									
									<div class="options_set">
										<label>Position</label>
										<div class="btn-group btn-group-xs" role="group" aria-label="...">
											<button type="button" class="btn btn-default"><i class="fa fa-chevron-up"></i></button>
											<button type="button" class="btn btn-default"><i class="fa fa-chevron-down"></i></button>
										</div>
									</div>
									<div class="options_set">
										<label>Always visible</label>
										<input type="checkbox" class="form-control" name="always_visible" value="on" checked="checked" />
									</div>
									<div class="options_set">
										<label>Discover DC</label>
										<input type="text" class="form-control single" name="discover_dc" value="15"/>
									</div>
									<div class="options_set">
										<label>Hitpoints</label>
										<input type="text" class="form-control single" name="hitpoints" value="20"/>
									</div>
									<div class="options_set">
										<label>Is locked</label>
										<input type="checkbox" class="form-control" name="locked" value="on" checked="checked" />
									</div>
									<div class="options_set">
										<label>Locked DC</label>
										<input type="text" class="form-control single" name="locked_dc" value="15"/>
									</div>
									<div class="options_set">
										<label>Is trapped</label>
										<input type="checkbox" class="form-control" name="trapped" value="on" checked="checked" />
									</div>
									<div class="options_set">
										<label>Trap detect DC</label>
										<input type="text" class="form-control single" name="trap_detect_dc" value="15"/>
									</div>
									<div class="options_set">
										<label>Trap disarm DC</label>
										<input type="text" class="form-control single" name="trap_disarm_dc" value="15"/>
									</div>
									<div class="options_set">
										<label class="lg">Description</label>
										<textarea class="form-control" name="description">Prop description</textarea>
									</div>
								</div>
							</div>
						</div>
						<div id="options_stairs">
							
						</div>
						<div id="options_traps">
							<div class="options_headline">Traps</div>
							<div class="options_info">select and click to place</div>
							<div class="proplist_wrapper">
								<?php echo $trapContent; ?>
							</div>
							<div class="advanced_options_wrapper">
								<div class="advanced_options_label">Advanced Options <i class="fa fa-plus"></i></div>
								<div class="advanced_options_inner">
									<div class="options_headline">Properties of</div>
									<div class="options_set">
										<label>Number</label>
										<input type="text" class="form-control single" name="door_number" value="1" />
									</div>
									<div class="options_set">
										<label>Name</label>
										<input type="text" class="form-control name single" name="door_name" value="Trap01" />
									</div>
									<div class="options_set">
										<label>Position</label>
										<div class="btn-group btn-group-xs" role="group" aria-label="...">
											<button type="button" class="btn btn-default"><i class="fa fa-chevron-up"></i></button>
											<button type="button" class="btn btn-default"><i class="fa fa-chevron-down"></i></button>
										</div>
									</div>
									<div class="options_set">
										<label>Visible on click</label>
										<input type="checkbox" class="form-control" name="always_visible" value="on" checked="checked" />
									</div>
									<div class="options_set">
										<label>Hitpoints</label>
										<input type="text" class="form-control single" name="hitpoints" value="20"/>
									</div>
									<div class="options_set">
										<label>Trap detect DC</label>
										<input type="text" class="form-control single" name="trap_detect_dc" value="15"/>
									</div>
									<div class="options_set">
										<label>Trap disarm DC</label>
										<input type="text" class="form-control single" name="trap_disarm_dc" value="15"/>
									</div>
									<div class="options_set">
										<label class="lg">Description</label>
										<textarea class="form-control" name="description">Prop description</textarea>
									</div>
								</div>
							</div>
						</div>
						<div id="options_notes">
							<div class="options_headline">Traps</div>
							<div class="options_set">
								<button type="button" class="edit_notes" data-toggle="modal" data-target="#notesModal">
									Edit Notes
								</button>
							</div>
						</div>
						<div id="options_links">
							<div class="options_headline">Links</div>
							<div class="options_set">
								<label>Number</label>
								<input type="text" class="form-control single" name="door_number" value="1" />
							</div>
							<div class="options_set">
								<label>Name</label>
								<input type="text" class="form-control name single" name="door_name" value="Link01" />
							</div>
							<div class="options_set">
								<label>Link to Map</label>
								<select name="link_to_map" class="form-control">
									<option value="map01">Map01</option>
									<option value="map02">Map02</option>
									<option value="map03">Map03</option>
									<option value="map04">Map04</option>
									<option value="map05">Map05</option>
								</select>
							</div>
							<div class="options_set">
								<label>Reveal Room</label>
								<select name="reveal_room" class="form-control">
									<option value="room01">Room01</option>
									<option value="room02">Room02</option>
									<option value="room03">Room03</option>
									<option value="room04">Room04</option>
									<option value="room05">Room05</option>
								</select>
							</div>
							
						</div>
						<div id="options_sounds">
						</div>
					</div>
					<!--
					<button id="options_ok_button" class="pull-right">OK</button>
					-->
				</div>
				</div>
			</div>
			
		</div>
		<div id="wallModal" class="modal fade" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Wall Background</h4>
					</div>
					<div class="modal-body">
						<?php echo $wallModalContent; ?>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default ok_button" data-dismiss="modal">OK</button>
					</div>
				</div>
			</div>
		</div>
		<div id="floorModal" class="modal fade" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Floor Background</h4>
					</div>
					<div class="modal-body">
						<?php echo $floorModalContent; ?>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default ok_button" data-dismiss="modal">OK</button>
					</div>
				</div>
			</div>
		</div>
		<div id="bgModal" class="modal fade" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Map Background</h4>
					</div>
					<div class="modal-body">
						<?php echo $bgModalContent; ?>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default ok_button" data-dismiss="modal">OK</button>
					</div>
				</div>
			</div>
		</div>
		<div id="notesModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="false">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Dungeon Master's Notes</h4>
					</div>
					<div class="modal-body">
						<div id="dm_notes">
							<div id="dm_notes_mapname">
								<span>Map01</span>
								<div class="dm_notes_hr"></div>
							</div>
							<div class="dm_notes_room">
								<div class="dm_notes_room_top">
									<span class="dm_notes_room_nr">1</span>
									<span class="dm_notes_room_name">Serving Room</span>
									<span class="dm_notes_room_show pull-right invis"></span>
									<span class="dm_notes_room_reveal pull-right reveal"></span>
									<i class="fa fa-chevron-down pull-right"></i>
								</div>
								<div class="dm_notes_room_notes">
									<div class="dm_notes_room_description description_box">
										<span class="room_icon icon"></span>
										<span class="room_description description">
											The serving rooms walls are made of cobblestone and the floor is made of old wood.<br/>
											2 doors are leading in this room.
										</span>
									</div>
									<div class="dm_notes_prop_description description_box">
										<span class="prop_icon icon"></span>
										<span class="prop_description description">
											This room contains: 1 Bed, 1 Chair and 1 Table
										</span>
									</div>
									<div class="dm_notes_doors_description description_box">
										<span class="door_icon icon"></span>
										<span class="door_description description">
											2 doors are leadin in this room.<br/>
											The western door is made of wood (HP 10), is unlocked and untrapped.<br/><br/>
											The eastern door is reinforced (HP 20), is locked (DC 15) and trapped (detect DC 17).<br/>
											Note: Fire trap deals 1d6 fire damage (halfed on save), disarm DC 20
										</span>
									</div>
									<div class="dm_notes_traps_description description_box">
										<span class="trap_icon icon"></span>
										<span class="trap_description description">
											There are 2 traps hidden in this room.<br/><br/>
											Fire Trap (detect DC 17, disarm DC 20)<br/>
											Note: ++At footside of bed ++ If triggered the trap releases a firebolt with a 30ft range (DEX save DC 12) and deals6d6 fire damage (halfed on save).<br/><br/>
											There are 2 traps hidden in this room.<br/><br/>
											Fire Trap (detect DC 17, disarm DC 20)<br/>
											Note: ++At side of bed ++ If triggered the trap releases a firebolt with a 30ft range (DEX save DC 12) and deals6d6 fire damage (halfed on save).<br/><br/>
											
										</span>
									</div>
									<div class="dm_notes_images clearfix">
										<div class="dm_note_image col-sm-6">
											<img src="./res/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/demonDretch.jpg" />
											<div class="dm_notes_button_wrapper">
												<button class="btn btn-default">
													show to all
												</button>
											</div>
										</div>
										<div class="dm_note_image col-sm-6">
											<img src="./res/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/Grell.jpg" />
											<div class="dm_notes_button_wrapper">
												<button class="btn btn-default">
													show to all
												</button>
											</div>
										</div>
										<div class="dm_note_image col-sm-6">
											<img src="./res/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/skeleton.jpg" />
											<div class="dm_notes_button_wrapper">
												<button class="btn btn-default">
													show to all
												</button>
											</div>
											
										</div>
									</div>
									<div class="dm_note_quote">
										<div class="dm_note_quote_inner">
											<p>
												A terrible beast is waiting in the shadows.
												<br/>
												As you enter the room it hisses at you and starts to run directly at you.
											</p>
										</div>
									</div>
									<div class="dm_notes_text">
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
										<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
										<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
										<p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.</p>
										<p>Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,</p>
									</div>
								</div>
							</div>
							<div class="dm_notes_room">
								<div class="dm_notes_room_top">
									<span class="dm_notes_room_nr">2</span>
									<span class="dm_notes_room_name">Kitchen</span>
									<span class="dm_notes_room_show pull-right invis"></span>
									<span class="dm_notes_room_reveal pull-right reveal"></span>
									<i class="fa fa-chevron-down pull-right"></i>
								</div>
								<div class="dm_notes_room_notes">
									<div class="dm_notes_room_description description_box">
										<span class="room_icon icon"></span>
										<span class="room_description description">
											The kitchen walls are made of cobblestone and the floor is made of old wood.<br/>
											2 doors are leading in this room.
										</span>
									</div>
									<div class="dm_notes_prop_description description_box">
										<span class="prop_icon icon"></span>
										<span class="prop_description description">
											This room contains: 1 Bed, 1 Chair and 1 Table
										</span>
									</div>
									<div class="dm_notes_doors_description description_box">
										<span class="door_icon icon"></span>
										<span class="door_description description">
											2 doors are leadin in this room.<br/>
											The western door is made of wood (HP 10), is unlocked and untrapped.<br/><br/>
											The eastern door is reinforced (HP 20), is locked (DC 15) and trapped (detect DC 17).<br/>
											Note: Fire trap deals 1d6 fire damage (halfed on save), disarm DC 20
										</span>
									</div>
									<div class="dm_notes_traps_description description_box">
										<span class="trap_icon icon"></span>
										<span class="trap_description description">
											There are 2 traps hidden in this room.<br/><br/>
											Fire Trap (detect DC 17, disarm DC 20)<br/>
											Note: ++At footside of bed ++ If triggered the trap releases a firebolt with a 30ft range (DEX save DC 12) and deals6d6 fire damage (halfed on save).<br/><br/>
											There are 2 traps hidden in this room.<br/><br/>
											Fire Trap (detect DC 17, disarm DC 20)<br/>
											Note: ++At side of bed ++ If triggered the trap releases a firebolt with a 30ft range (DEX save DC 12) and deals6d6 fire damage (halfed on save).<br/><br/>
											
										</span>
									</div>
									<div class="dm_notes_images clearfix">
										<div class="dm_note_image col-sm-6">
											<img src="./res/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/demonDretch.jpg" />
											<div class="dm_notes_button_wrapper">
												<button class="btn btn-default">
													show to all
												</button>
											</div>
										</div>
										<div class="dm_note_image col-sm-6">
											<img src="./res/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/Grell.jpg" />
											<div class="dm_notes_button_wrapper">
												<button class="btn btn-default">
													show to all
												</button>
											</div>
										</div>
										<div class="dm_note_image col-sm-6">
											<img src="./res/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/skeleton.jpg" />
											<div class="dm_notes_button_wrapper">
												<button class="btn btn-default">
													show to all
												</button>
											</div>
											
										</div>
									</div>
									<div class="dm_note_quote">
										<div class="dm_note_quote_inner">
											<p>
												A terrible beast is waiting in the shadows.
												<br/>
												As you enter the room it hisses at you and starts to run directly at you.
											</p>
										</div>
									</div>
									<div class="dm_notes_text">
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
										<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
										<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
										<p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.</p>
										<p>Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,</p>
									</div>
								</div>
							</div>
							<div class="dm_notes_room">
								<div class="dm_notes_room_top">
									<span class="dm_notes_room_nr">3</span>
									<span class="dm_notes_room_name">Dining Room</span>
									<span class="dm_notes_room_show pull-right invis"></span>
									<span class="dm_notes_room_reveal pull-right reveal"></span>
									<i class="fa fa-chevron-down pull-right"></i>
								</div>
								<div class="dm_notes_room_notes">
									<div class="dm_notes_room_description description_box">
										<span class="room_icon icon"></span>
										<span class="room_description description">
											The dining room walls are made of cobblestone and the floor is made of old wood.<br/>
											2 doors are leading in this room.
										</span>
									</div>
									<div class="dm_notes_prop_description description_box">
										<span class="prop_icon icon"></span>
										<span class="prop_description description">
											This room contains: 1 Bed, 1 Chair and 1 Table
										</span>
									</div>
									<div class="dm_notes_doors_description description_box">
										<span class="door_icon icon"></span>
										<span class="door_description description">
											2 doors are leadin in this room.<br/>
											The western door is made of wood (HP 10), is unlocked and untrapped.<br/><br/>
											The eastern door is reinforced (HP 20), is locked (DC 15) and trapped (detect DC 17).<br/>
											Note: Fire trap deals 1d6 fire damage (halfed on save), disarm DC 20
										</span>
									</div>
									<div class="dm_notes_traps_description description_box">
										<span class="trap_icon icon"></span>
										<span class="trap_description description">
											There are 2 traps hidden in this room.<br/><br/>
											Fire Trap (detect DC 17, disarm DC 20)<br/>
											Note: ++At footside of bed ++ If triggered the trap releases a firebolt with a 30ft range (DEX save DC 12) and deals6d6 fire damage (halfed on save).<br/><br/>
											There are 2 traps hidden in this room.<br/><br/>
											Fire Trap (detect DC 17, disarm DC 20)<br/>
											Note: ++At side of bed ++ If triggered the trap releases a firebolt with a 30ft range (DEX save DC 12) and deals6d6 fire damage (halfed on save).<br/><br/>
											
										</span>
									</div>
									<div class="dm_notes_images clearfix">
										<div class="dm_note_image col-sm-6">
											<img src="./res/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/demonDretch.jpg" />
											<div class="dm_notes_button_wrapper">
												<button class="btn btn-default">
													show to all
												</button>
											</div>
										</div>
										<div class="dm_note_image col-sm-6">
											<img src="./res/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/Grell.jpg" />
											<div class="dm_notes_button_wrapper">
												<button class="btn btn-default">
													show to all
												</button>
											</div>
										</div>
										<div class="dm_note_image col-sm-6">
											<img src="./res/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/skeleton.jpg" />
											<div class="dm_notes_button_wrapper">
												<button class="btn btn-default">
													show to all
												</button>
											</div>
											
										</div>
									</div>
									<div class="dm_note_quote">
										<div class="dm_note_quote_inner">
											<p>
												A terrible beast is waiting in the shadows.
												<br/>
												As you enter the room it hisses at you and starts to run directly at you.
											</p>
										</div>
									</div>
									<div class="dm_notes_text">
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
										<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
										<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
										<p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.</p>
										<p>Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,</p>
									</div>
								</div>
							</div>
							<div class="dm_notes_room">
								<div class="dm_notes_room_top">
									<span class="dm_notes_room_nr">4</span>
									<span class="dm_notes_room_name">Living Room</span>
									<span class="dm_notes_room_show pull-right invis"></span>
									<span class="dm_notes_room_reveal pull-right reveal"></span>
									<i class="fa fa-chevron-down pull-right"></i>
								</div>
								<div class="dm_notes_room_notes">
									<div class="dm_notes_room_description description_box">
										<span class="room_icon icon"></span>
										<span class="room_description description">
											The living room walls are made of cobblestone and the floor is made of old wood.<br/>
											2 doors are leading in this room.
										</span>
									</div>
									<div class="dm_notes_prop_description description_box">
										<span class="prop_icon icon"></span>
										<span class="prop_description description">
											This room contains: 1 Bed, 1 Chair and 1 Table
										</span>
									</div>
									<div class="dm_notes_doors_description description_box">
										<span class="door_icon icon"></span>
										<span class="door_description description">
											2 doors are leadin in this room.<br/>
											The western door is made of wood (HP 10), is unlocked and untrapped.<br/><br/>
											The eastern door is reinforced (HP 20), is locked (DC 15) and trapped (detect DC 17).<br/>
											Note: Fire trap deals 1d6 fire damage (halfed on save), disarm DC 20
										</span>
									</div>
									<div class="dm_notes_traps_description description_box">
										<span class="trap_icon icon"></span>
										<span class="trap_description description">
											There are 2 traps hidden in this room.<br/><br/>
											Fire Trap (detect DC 17, disarm DC 20)<br/>
											Note: ++At footside of bed ++ If triggered the trap releases a firebolt with a 30ft range (DEX save DC 12) and deals6d6 fire damage (halfed on save).<br/><br/>
											There are 2 traps hidden in this room.<br/><br/>
											Fire Trap (detect DC 17, disarm DC 20)<br/>
											Note: ++At side of bed ++ If triggered the trap releases a firebolt with a 30ft range (DEX save DC 12) and deals6d6 fire damage (halfed on save).<br/><br/>
											
										</span>
									</div>
									<div class="dm_notes_images clearfix">
										<div class="dm_note_image col-sm-6">
											<img src="./res/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/demonDretch.jpg" />
											<div class="dm_notes_button_wrapper">
												<button class="btn btn-default">
													show to all
												</button>
											</div>
										</div>
										<div class="dm_note_image col-sm-6">
											<img src="./res/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/Grell.jpg" />
											<div class="dm_notes_button_wrapper">
												<button class="btn btn-default">
													show to all
												</button>
											</div>
										</div>
										<div class="dm_note_image col-sm-6">
											<img src="./res/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/skeleton.jpg" />
											<div class="dm_notes_button_wrapper">
												<button class="btn btn-default">
													show to all
												</button>
											</div>
											
										</div>
									</div>
									<div class="dm_note_quote">
										<div class="dm_note_quote_inner">
											<p>
												A terrible beast is waiting in the shadows.
												<br/>
												As you enter the room it hisses at you and starts to run directly at you.
											</p>
										</div>
									</div>
									<div class="dm_notes_text">
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
										<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
										<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
										<p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.</p>
										<p>Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,</p>
									</div>
								</div>
							</div>
							<div class="dm_notes_room">
								<div class="dm_notes_room_top">
									<span class="dm_notes_room_nr">5</span>
									<span class="dm_notes_room_name">Storage Room</span>
									<span class="dm_notes_room_show pull-right invis"></span>
									<span class="dm_notes_room_reveal pull-right reveal"></span>
									<i class="fa fa-chevron-down pull-right"></i>
								</div>
								<div class="dm_notes_room_notes">
									<div class="dm_notes_room_description description_box">
										<span class="room_icon icon"></span>
										<span class="room_description description">
											The storage room walls are made of cobblestone and the floor is made of old wood.<br/>
											2 doors are leading in this room.
										</span>
									</div>
									<div class="dm_notes_prop_description description_box">
										<span class="prop_icon icon"></span>
										<span class="prop_description description">
											This room contains: 1 Bed, 1 Chair and 1 Table
										</span>
									</div>
									<div class="dm_notes_doors_description description_box">
										<span class="door_icon icon"></span>
										<span class="door_description description">
											2 doors are leadin in this room.<br/>
											The western door is made of wood (HP 10), is unlocked and untrapped.<br/><br/>
											The eastern door is reinforced (HP 20), is locked (DC 15) and trapped (detect DC 17).<br/>
											Note: Fire trap deals 1d6 fire damage (halfed on save), disarm DC 20
										</span>
									</div>
									<div class="dm_notes_traps_description description_box">
										<span class="trap_icon icon"></span>
										<span class="trap_description description">
											There are 2 traps hidden in this room.<br/><br/>
											Fire Trap (detect DC 17, disarm DC 20)<br/>
											Note: ++At footside of bed ++ If triggered the trap releases a firebolt with a 30ft range (DEX save DC 12) and deals6d6 fire damage (halfed on save).<br/><br/>
											There are 2 traps hidden in this room.<br/><br/>
											Fire Trap (detect DC 17, disarm DC 20)<br/>
											Note: ++At side of bed ++ If triggered the trap releases a firebolt with a 30ft range (DEX save DC 12) and deals6d6 fire damage (halfed on save).<br/><br/>
											
										</span>
									</div>
									<div class="dm_notes_images clearfix">
										<div class="dm_note_image col-sm-6">
											<img src="./res/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/demonDretch.jpg" />
											<div class="dm_notes_button_wrapper">
												<button class="btn btn-default">
													show to all
												</button>
											</div>
										</div>
										<div class="dm_note_image col-sm-6">
											<img src="./res/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/Grell.jpg" />
											<div class="dm_notes_button_wrapper">
												<button class="btn btn-default">
													show to all
												</button>
											</div>
										</div>
										<div class="dm_note_image col-sm-6">
											<img src="./res/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/skeleton.jpg" />
											<div class="dm_notes_button_wrapper">
												<button class="btn btn-default">
													show to all
												</button>
											</div>
											
										</div>
									</div>
									<div class="dm_note_quote">
										<div class="dm_note_quote_inner">
											<p>
												A terrible beast is waiting in the shadows.
												<br/>
												As you enter the room it hisses at you and starts to run directly at you.
											</p>
										</div>
									</div>
									<div class="dm_notes_text">
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
										<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
										<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
										<p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.</p>
										<p>Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default ok_button" data-dismiss="modal">OK</button>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
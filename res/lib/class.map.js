class Map{
	constructor(){
		this._name = 'Map';
		this._setting = 'fantasy';
		this._gridType = 'square';
		this._gridSize = 0;
		this._width = 0;
		this._height = 0;
		this._unitDistance = 5;
		this._unitMeasure = 'ft';
		this._backgroundImage = '';
		this._backgroundStyle = 'color';
		this._backgroundColor = '#000000';
		this._backgroundScale = '100';
		this._snapToGrid = false;
		//mapProps = {};
		//rooms = {};
		//daytime = 12;
	}
	set name(n){
	  this._name = n;
	}
	get name(){
	  return this._name;
	}
	set setting(s) {
	  this._setting = s;
	}
	get setting(){
	  return this._setting;
	}
	set gridType(gt) {
	  this._gridType = gt;
	}
	get gridType(){
	  return this._gridType;
	}
	set gridSize(gs) {
	  this._gridSize = gs;
	}
	get gridSize(){
	  return this._gridSize;
	}
	set width(w) {
	  this._width = w;
	}
	get width(){
	  return this._width;
	}
	set height(h) {
		this._height = h;
	}
	get height(){
	  return this._height;
	}
	set unitDistance(ud) {
	  this._unitDistance = ud;
	}
	get unitDistance(){
	  return this._unitDistance;
	}
	set unitMeasure(um) {
	  this._unitMeasure = um;
	}
	get unitMeasure(){
	  return this._unitMeasure;
	}
	set backgroundImage(bg) {
	  this._backgroundImage = bg;
	}
	get backgroundImage(){
	  return this._backgroundImage;
	}
	set backgroundStyle(bs) {
	  this._backgroundStyle = bs;
	}
	get backgroundStyle(){
	  return this._backgroundStyle;
	}
	set backgroundColor(bc) {
	  this._backgroundColor = bc;
	}
	get backgroundColor(){
	  return this._backgroundColor;
	}
	set backgroundScale(bs) {
	  this._backgroundScale = bs;
	}
	get backgroundScale(){
	  return this._backgroundScale;
	}
	set snapToGrid(sg) {
	  this._snapToGrid = sg;
	}
	get snapToGrid(){
	  return this._snapToGrid;
	}
	//function setDaytime(dt) {
	//	daytime = dt;
	//}
	
	
	
	
	
	
	/*
	function initMap(map) {
		width = map.width;
		height = map.height;
		backgroundImage = map.backgroundImage;
		//mapProps = map.mapProps;
		gridSize = map.gridSize;
		gridType = map.gridType;
		//rooms = map.rooms;
		setting = map.setting;
		//daytime = map.daytime;
	}
	*/
	get objAsJSON() {
		return '{}';
	}
}
$(document).ready(function(){
  //activate linetype
  $('.line_type').on('click', function(){
	$('.line_type').removeClass('active');
	$(this).addClass('active');
  });
  
  //activate tools
  $('.tool_icon').on('click', function(){
	if (!$(this).hasClass('disabled')) {
	  if ($(this).hasClass('active')) {
		  $(this).removeClass('active');
	  }else{
		$('.tool_icon').removeClass('active');
		$(this).addClass('active');
	  }
	}
  });
  
  //show/hide propsets
  $('.option_cat').on('click', function(){
	if (!$(this).hasClass('disabled')) {
	  if ($(this).hasClass('active')) {
		$(this).removeClass('active');
		$('#options').removeClass('open');
		$('#options_inner > div').removeClass('active');
	  }else{
		$('.option_cat').removeClass('active');
		$(this).addClass('active');
		$('#options').addClass('open');
		$('#options_inner > div').removeClass('active');
		var toOpen = $(this).attr('id').split('_');
		$('#options_'+toOpen[1]).addClass('active');
	  }
	}
  });
  
  //show/hide advanced options
  $('.advanced_options_label').on('click', function(){
	$(this).find('i').toggleClass('fa-plus fa-minus');
	$(this).next().toggle();
  });
  
  //Set Icon to pattern on default/load
  if ($('.floor_pattern').length > 0) {
	var currentPattern = $('.floor_pattern').data('pattern');
	$('.lg').each(function(){
	  if($(this).data('pattern') == currentPattern){
		$(this).append("<span class='glyphicon glyphicon-ok'></span>");
	  }
	});
  }
  if ($('.wall_pattern').length > 0) {
	var currentPattern = $('.wall_pattern').data('pattern');
	$('.lg').each(function(){
	  if($(this).data('pattern') == currentPattern){
		$(this).append("<span class='glyphicon glyphicon-ok'></span>");
	  }
	});
  }
  if ($('#background_pattern').length > 0) {
	var currentPattern = $('#background_pattern').data('pattern');
	$('.lg').each(function(){
	  if($(this).data('pattern') == currentPattern){
		$(this).append("<span class='glyphicon glyphicon-ok'></span>");
	  }
	});
  }
  
  //choose pattern
  $('.pattern.lg').on('click', function(){
	var parent = $(this).parent().parent().parent();
	parent.find('span').remove();
	$(this).append("<span class='glyphicon glyphicon-ok'></span>");
	var newPattern = $(this).data('pattern');
	var selector = '.'+$(this).data('selector');
	var url = $(selector).attr('style').split('"');
	var urlPartsArr = url[1].split("/");
	var newUrl = '';
	for(var i = 0; i < urlPartsArr.length-1; i++){
	  newUrl += urlPartsArr[i]+"/";
	}
	$(selector).css('background-image', "url('"+newUrl+newPattern+"')");
	$(selector).attr('data-pattern', newPattern);
	
  });
  
  //shadow toggle
  $('.toggle_shadow').on('click', function(){
	$(this).find('span').each(function(){
	  switch($(this).attr('class')){
		case 'shadow_on':
		  $('.shadow_on').removeClass('shadow_on').addClass('shadow_off').text('OFF');
		  break;
		case 'shadow_off':
		  $('.shadow_off').removeClass('shadow_off').addClass('shadow_on').text('ON');
		  break;
	  }
	});
  });
  
  //change background settings
  $('select[name="background_type"]').on('change', function(){
	switch($(this).val()){
	  case 'pattern':
		//enable pattern
		$('input[name="background_patter_size"]').prop('disabled', false);
		$('input[name="background_patter_size"]').prev().removeClass('disabled');
		$('input[name="background_patter_size"]').parent().removeClass('disabled');
		$('input[name="background_patter_size"]').prev().attr('data-target', '#bgModal');
		
		//disable color
		$('input[name="background_color"]').prop('disabled', true);
		$('input[name="background_color"]').parent().addClass('disabled');
		
		//disable image
		$('input[name="choose_bg_file"]').prop('disabled', true);
		$('input[name="choose_bg_file"]').prev().find('span').addClass('disabled');
		$('input[name="choose_bg_file"]').prev().find('input').prop('disabled', true);
		break;
	  case 'color':
		//enable color
		$('input[name="background_color"]').prop('disabled', false);
		$('input[name="background_color"]').parent().removeClass('disabled');
		
		//disable pattern
		$('input[name="background_patter_size"]').prop('disabled', true);
		$('input[name="background_patter_size"]').prev().addClass('disabled');
		$('input[name="background_patter_size"]').parent().addClass('disabled');
		$('input[name="background_patter_size"]').prev().attr('data-target', '');
		
		//disable image
		$('input[name="choose_bg_file"]').prop('disabled', true);
		$('input[name="choose_bg_file"]').prev().find('span').addClass('disabled');
		$('input[name="choose_bg_file"]').prev().find('input').prop('disabled', true);
		break;
	  case 'image':
		//enable image
		$('input[name="choose_bg_file"]').prop('disabled', false);
		$('input[name="choose_bg_file"]').prev().find('span').removeClass('disabled');
		$('input[name="choose_bg_file"]').prev().find('input').prop('disabled', false);
		
		//disable pattern
		$('input[name="background_patter_size"]').prop('disabled', true);
		$('input[name="background_patter_size"]').prev().addClass('disabled');
		$('input[name="background_patter_size"]').parent().addClass('disabled');
		$('input[name="background_patter_size"]').prev().attr('data-target', '');
		
		//disable color
		$('input[name="background_color"]').prop('disabled', true);
		$('input[name="background_color"]').parent().addClass('disabled');
		break;
	}
  });
  
  //text-input-steps
  $("input.touchspin").TouchSpin({
      verticalbuttons: true,
	  step: 50,
	  min: 0,
	  max: 500
    });
  
  //show/hide right sidebar
  $('#cat_collapse').on('click', function(){
	if (!$('.option_cat').hasClass('active')) {
	  $('#cat_settings').addClass('active');
	  $('#options').addClass('open');
	  $('#options_settings').addClass('active');
	}else{
	  if ($('#options').is(':visible')) {
		$('#options').removeClass('open');
	  }else{
		$('#options').addClass('open');
	  }
	}
  });
  
  //show/hide proplists
  $('.proplist_headline').on('click', function(){
	$(this).find('i').toggleClass('fa-plus fa-minus');
	
	if ($(this).next().is(':visible')) {
	  $(this).next().slideUp();
	}else{
	  $(this).next().slideDown();
	}
  });
  
  //set prop active
  $('.prop_inner').on('click', function(){
	$('.prop_inner').removeClass('active');
	$(this).addClass('active');
  });
  
  /* dm notes */
  //show/hide room content
  $('.dm_notes_room_top > i').on('click', function(){
	$(this).toggleClass('fa-chevron-down fa-chevron-up');
	if ($(this).parent().next('.dm_notes_room_notes').is(':visible')) {
	  $(this).parent().next('.dm_notes_room_notes').slideUp();
	}else{
	  $(this).parent().next('.dm_notes_room_notes').slideDown();
	}
  });
  
  //reveal toggle
  $('.dm_notes_room_reveal').on('click', function(){
	$(this).toggleClass('reveal revealed');
  });
  
  //show/hide toggle
  $('.dm_notes_room_show').on('click', function(){
	$(this).toggleClass('vis invis');
  });
  
  //make DM-Notes modal-container resizable
  $('.modal-content').resizable({
	//alsoResize: ".modal-dialog",
	minHeight: 300,
	minWidth: 300
  });
  //make DM-Notes modal-container dragable
  $('.modal-dialog').draggable();
  //disable drag while scrolling the content
  $(".modal-body").mousedown(function(event) {
	event.stopPropagation();
  });
  
  //make DM-Note Content sortable
  $( ".dm_notes_room_notes" ).sortable({
	placeholder: "ui-state-highlight"
  });
  $( ".dm_notes_room_notes" ).disableSelection();
  
});